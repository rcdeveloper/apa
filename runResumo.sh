#/bash

nAlgorithm=1
nfile=1
pwd

while [[ $nAlgorithm != "6" ]]; do
	 if [[ $nAlgorithm = "1" ]];
  then
   	 algorithmName="SelectionSort"
	
	elif [[ $nAlgorithm = "2" ]];
 then
		algorithmName="InsertionSort"
	
	elif [[ $nAlgorithm = "3" ]];
 then
		algorithmName="MergeSort"

	elif [[ $nAlgorithm = "4" ]];
 then
		algorithmName="QuickSort"

elif [[ $nAlgorithm = "5" ]];
 then
		algorithmName="HeapSort"

  fi	


echo "Coletando $algorithmName"

input="saida/$algorithmName"
saida="saida/dados"
###############################################
############# 10% de Ordenação ################
###############################################

echo "$algorithmName [10%][100.000]"
while [[ $nfile != "11" ]]; do

    echo "arquivo [" 10.100000.$nfile.out "] adicionado"

    cat $input/10.100000.$nfile.out >> $saida/$algorithmName.10.100000.out
    ((nfile=$nfile+1))
done
 nfile=1


echo "$algorithmName [10%][500.000]"
while [[ $nfile != "11" ]]; do

    echo "arquivo [" 10.500000.$nfile.out "] adicionado"
  
    cat $input/10.500000.$nfile.out >> $saida/$algorithmName.10.500000.out
    ((nfile=$nfile+1))
done
 nfile=1


 
echo "$algorithmName [10%][1.000.000]"
 
while [[ $nfile != "11" ]]; do

    echo "arquivo [" 10.1000000.$nfile.out "] adicionado"
   
    cat $input/10.1000000.$nfile.out >> $saida/$algorithmName.10.1000000.out
    ((nfile=$nfile+1))
done
 
###############################################
############# 50% de Ordenação ################
###############################################
 
nfile=1


echo "$algorithmName [50%][100.000]"
 
while [[ $nfile != "11" ]]; do

    echo "arquivo [" 50.100000.$nfile.out "] adicionado"
    
    cat $input/50.100000.$nfile.out >> $saida/$algorithmName.50.100000.out
    ((nfile=$nfile+1))
done
 
nfile=1

 
echo "$algorithmName [50%][500.000]"
 
while [[ $nfile != "11" ]]; do

    echo "arquivo [" 50.500000.$nfile.out "] adicionado"
   
    cat $input/50.500000.$nfile.out >> $saida/$algorithmName.50.500000.out
    ((nfile=$nfile+1))
done
 
nfile=1

 
echo "$algorithmName [50%][1.000.000]"
 
while [[ $nfile != "11" ]]; do

    echo "arquivo [" 50.1000000.$nfile.out "] adicionado"
     
    cat $input/50.1000000.$nfile.out >> $saida/$algorithmName.50.1000000.out
    ((nfile=$nfile+1))
done
 
###############################################
############# 90% de Ordenação ################
###############################################
 
nfile=1

 
echo "$algorithmName [90%][100.000]"
while [[ $nfile != "11" ]]; do

    echo "arquivo [" 90.100000.$nfile.out "] adicionado"
    #" "
    cat $input/90.100000.$nfile.out >> $saida/$algorithmName.90.100000.out
    ((nfile=$nfile+1))
done
 
nfile=1
 
echo "$algorithmName [90%][500.000]"
 
while [[ $nfile != "11" ]]; do

    echo "arquivo [" 90.500000.$nfile.out "] adicionado"
     
    cat $input/90.500000.$nfile.out >> $saida/$algorithmName.90.500000.out
    ((nfile=$nfile+1))
done
 
nfile=1
 
echo "$algorithmName [90%][1.000.000]"
 
while [[ $nfile != "11" ]]; do

    echo "arquivo [" 90.1000000.$nfile.out "] adicionado"
    
    cat $input/90.1000000.$nfile.out >> $saida/$algorithmName.90.1000000.out
    ((nfile=$nfile+1))
done
 nfile=1
 ((nAlgorithm=$nAlgorithm+1))

done
