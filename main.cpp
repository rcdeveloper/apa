#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>
#include <chrono>
#include <cstdlib>


typedef struct lista {
    struct no *cabeca;
    int tamanho;
    int tamanho_alocado;
} tLista;



typedef struct no {
    int conteudo;
    struct no *proximo;
} tNo;

// Péssimo caminho, mas só descobri na metade. Preferi ver como era usando vetor.
/**
typedef struct heap {
    struct noHeap *pai;

} tHeap;

typedef struct noHeap {
    int conteudo;
    struct noHeap  *noLeft;
    struct noHeap  *noRight;
} tHeapNo;
**/


void cria(tLista *lista);
int cria(tLista *lista, int tamanho);
int vazia(tLista lista);
int tamanho(tLista lista);
int valor(tLista lista, int posicao, int *valor);
int modifica(tLista *lista, int posicao, int valor);
int insere(tLista *lista, int posicao, int valor);
int insereListaVazia(tLista *lista, int valor);
int insereInicioLista(tLista *lista, int valor);
int insereMeioOuFim(tLista *lista, int posicao, int valor);
int retira(tLista *lista, int posicao, int *conteudo);
int orderSelectionSort(tLista *lista);
int insertionSort(tLista *lista, int valor);
int orderMergeSort(tLista *lista);
tNo* orderMergeSortNo(tNo *leftNo, tNo *rightNo, int tamanho);

int orderQuickSort(tLista *lista);
tNo* orderQuickSortNo(tNo *pivoNo, tNo *subListNo);
// Desisti desse caminho quando vi o trabalho q ia me dar.
/**
int insereHeap(tHeap *pai, int valores[], int n);

int printHeap(tHeapNo *no);
int montaHeap(tHeapNo *heaPai, int vetor[], int n, int i);
**/
void max_heapify(int vet[], int n, int i);
void build_max_heap(int vet[], int n);
void heap_sort(int vet[], int n);

using namespace std;


void cria(tLista *lista) {
    lista->cabeca=NULL;
    lista->tamanho=0;
}

int cria(tLista *lista, int tamanho) {
    lista->cabeca=NULL;
    lista->tamanho=0;
    lista->cabeca=new tNo;
    tNo *no=lista->cabeca;
    no->conteudo=0;
    int i=1;
    for(; i<tamanho; i++) {

        no->proximo=new tNo;
        no=no->proximo;
        no->proximo=NULL;
        no->conteudo=0;

    }

    lista->tamanho_alocado=tamanho;

    return 1;
}

int vazia(tLista lista) {

    if(lista.tamanho==0) {
        return 1;
    } else {
        return 0;
    }
}

int tamanho(tLista lista) {
    return lista.tamanho;
}



int valor(tLista lista, int posicao, int *valor) {
    if(posicao>lista.tamanho) {
        return 0;
    } else {
        int i=1;
        tNo *no=lista.cabeca;
        while(i<posicao) {
            no=no->proximo;
            i++;
        }
        *valor=no->conteudo;
        return 1;
    }
}

int modifica(tLista *lista, int posicao, int valor) {
    if(posicao>lista->tamanho) {
        return 0;
    } else {
        int i=1;
        tNo *no=lista->cabeca;
        while(i<posicao) {
            no=no->proximo;
            i++;
        }
        no->conteudo=valor;
        return 1;
    }
}

int insere(tLista *lista, int posicao, int valor) {


    return insereMeioOuFim(lista, posicao, valor);

}

int insereListaVazia(tLista *lista, int valor) {
    tNo *no=new tNo;
    if(no==NULL) {
        return 0;
    }
    no->conteudo=valor;
    no->proximo=NULL;
    lista->cabeca=no;
    lista->tamanho++;
    return 1;
}

int insereInicioLista(tLista *lista, int valor) {
    tNo *no=new tNo;
    if(no==NULL) {
        return 0;
    } else {
        no->conteudo=valor;
        no->proximo=lista->cabeca;
        lista->cabeca=no;
        lista->tamanho++;
        return 1;
    }
}


int insereMeioOuFim(tLista *lista, int posicao, int valor) {

    // a posição passada por parametro n serve de nada nessa implementação!!!!!!!!!!!!!!
    int tamanho=lista->tamanho;
    int i=0;
    tNo *no=lista->cabeca;
    for(; i<tamanho; i++) {

        no=no->proximo;

    }
    no->conteudo=valor;
    lista->tamanho++;
    return 1;


}
int retira(tLista *lista, int posicao, int *conteudo) {

    tNo *novoNo=new tNo;
    if(novoNo==NULL) {
        return 0;
    }

    tNo *no=lista->cabeca;
    if(no==NULL) {
        return 0;
    }

    if(posicao==1) {
        tNo *novoNo=no->proximo;
        *conteudo=no->conteudo;
        delete lista->cabeca;
        lista->cabeca=novoNo;
        lista->tamanho--;

        return 1;
    }


    int i=1;
    while(i<posicao-1) {
        no=no->proximo;
        i++;
    }

    novoNo=no->proximo;
    *conteudo=novoNo->conteudo;
    no->proximo=novoNo->proximo;
    delete novoNo;
    lista->tamanho--;
    return 1;

}

int orderSelectionSort(tLista *lista) {

    int analizandoElemento=1;
    int auxTroca;
    tNo *noAnalizado=lista->cabeca;
    tNo *no=noAnalizado->proximo;
    tNo *noMenorValor=NULL;

    for(analizandoElemento=1; analizandoElemento<lista->tamanho; analizandoElemento++) {
        //  printf("analizando :%d\n", analizandoElemento);
        noMenorValor=noAnalizado;
        no=noAnalizado->proximo;
        while(no!=NULL) {
            if(no->conteudo<=noMenorValor->conteudo) {
                noMenorValor=no;
            }
            no=no->proximo;
        }
        auxTroca=noMenorValor->conteudo;
        noMenorValor->conteudo=noAnalizado->conteudo;
        noAnalizado->conteudo=auxTroca;
        noAnalizado=noAnalizado->proximo;
    }

    return 1;

}


int insertionSort(tLista *lista, int valor) {

    int tamanho=lista->tamanho;

    // printf("ANALISANDO ONDE INSERIR O VALOR %d\n", valor);

    tNo *no=lista->cabeca;

    if(tamanho==0) {

        no->conteudo=valor;
        lista->tamanho++;
        return 1;
    }


    int i=0;
    for(; i<tamanho-1; i++) {

        no=no->proximo;

    }

    //  printf(" tamanho %d\n", tamanho);

    tNo *novoNo=no->proximo;

    //  printf(" valor presente no novoNO %d\n", novoNo->conteudo);


    no->proximo=novoNo->proximo;
    //  novoNo->conteudo=valor;
    novoNo->proximo=NULL;

    no=lista->cabeca;
    i=0;
    while(i<=lista->tamanho-1) {
        if(valor<=no->conteudo) {
            //        printf("entrei pro valor %d\n", valor);
            novoNo->proximo=no->proximo;
            no->proximo=novoNo;
            novoNo->conteudo=no->conteudo;
            no->conteudo=valor;
            lista->tamanho++;
            return 1;
        }
        if(i==lista->tamanho-1) {

            break;
        } else {
            no=no->proximo;
        }
        i++;
    }

    novoNo->conteudo=valor;
    novoNo->proximo=no->proximo;
    no->proximo=novoNo;
    lista->tamanho++;



    return 1;



}

int orderMergeSort(tLista *lista) {

    int aux=(int)lista->tamanho / 2;
    //   printf("metade do tamanho: %d\n",aux);

    if(aux>=1) {
        int i=1;
        tNo *no=lista->cabeca;
        while(i<aux) {
            no=no->proximo;
            i++;
        }
        tNo *no2=no->proximo;
        no->proximo=NULL;
        //    printf("primeiro valor: subLeft %d , subRight  %d\n",lista->cabeca->conteudo, no2->conteudo);

        lista->cabeca=orderMergeSortNo(lista->cabeca, no2, aux);


        return 1;

    }

    return -1;

}

tNo* orderMergeSortNo(tNo *leftNo, tNo *rightNo, int tamanho) {


    // printf("variavel tamanho: %d\n", tamanho);
    int aux=(int)(tamanho)/2;
    // printf("variavel aux: %d\n", aux);

    //printa as listas
    if(false) {
        printf("lista da esquerda: \n");

        tNo *noDoPrint=leftNo;
        while(noDoPrint!=NULL) {
            printf("%d \n", noDoPrint->conteudo);
            noDoPrint=noDoPrint->proximo;

        }

        printf("lista da direita: \n");

        noDoPrint=rightNo;
        while(noDoPrint!=NULL) {
            printf("%d \n", noDoPrint->conteudo);
            noDoPrint=noDoPrint->proximo;

        }
    }

    if(rightNo->proximo!=NULL) {
        int i=1;
        tNo *no;
        tNo *no2;
        if(aux>0) {
            no=leftNo;
            no2=NULL;
            while(i<aux) {
                //   printf("valor de i dentro do while= %d \n",i);
                no=no->proximo;
                i++;
            }
            no2=no->proximo;
            no->proximo=NULL;
//printf("primeiro valor: subLeft %d , subRight  %d\n",leftNo->conteudo, no2->conteudo);

            leftNo=orderMergeSortNo(leftNo, no2, aux);
        }
        if(false) {
            printf("leftNo ordenado: \n");

            tNo *noDoPrint=leftNo;
            while(noDoPrint!=NULL) {
                printf("%d \n", noDoPrint->conteudo);
                noDoPrint=noDoPrint->proximo;

            }
        }
//leftNo->proximo=rightNo;
        no=rightNo;
        no2=NULL;
        i=1;
        while(i<aux) {
            no=no->proximo;
            i++;
        }
        no2=no->proximo;
        no->proximo=NULL;
//        printf("primeiro valor: subLeft %d , subRight  %d\n",rightNo->conteudo, no2->conteudo);

        rightNo=orderMergeSortNo(rightNo, no2, aux);
    } else {
        if(leftNo->conteudo<=rightNo->conteudo) {
            leftNo->proximo=rightNo; /// OPA, VERIFICAR ISSO AQUI
            rightNo->proximo=NULL;
        } else {
            tNo *noAux=NULL;
            noAux=leftNo;
            leftNo=rightNo;
            rightNo=noAux;
            leftNo->proximo=rightNo;
            rightNo->proximo=NULL;
        }
        return leftNo;
    }
    tNo *noAux=NULL;
    int elementosDaDireita=0;
    if(false) {
        printf("PRINTS ordenados: \n");

        tNo *noDoPrint=leftNo;
        while(noDoPrint!=NULL) {
            printf("esquerda %d \n", noDoPrint->conteudo);
            noDoPrint=noDoPrint->proximo;

        }
        noDoPrint=rightNo;
        while(noDoPrint!=NULL) {
            printf("direita %d \n", noDoPrint->conteudo);
            noDoPrint=noDoPrint->proximo;

        }
    }

    while(rightNo!=NULL) {
        elementosDaDireita++;
        noAux=rightNo;
        //     if(rightNo->proximo!=NULL){
        rightNo=rightNo->proximo;
        //   }
        tNo *noAux2=leftNo;
        //     printf("valores: %d e %d\n",noAux->conteudo,noAux2->conteudo);
        if(noAux->conteudo<=noAux2->conteudo) {
            noAux->proximo=noAux2;
            leftNo=noAux;
            continue;
        } else {
            if(noAux2->proximo!=NULL) {

                while(noAux->conteudo>noAux2->proximo->conteudo) {

                    noAux2=noAux2->proximo;
                    if(noAux2->proximo==NULL) {
                        break;
                    }
                }

            }
            noAux->proximo=noAux2->proximo;
            noAux2->proximo=noAux;
            //   leftNo=noAux2;
            //    tNo *noDoPrint=leftNo;
//printf("\n");
            //    while(noDoPrint!=NULL) {
            //              printf("esquerda %d \n", noDoPrint->conteudo);
            //        noDoPrint=noDoPrint->proximo;
//
            //      }

        }

    }
    return leftNo;
}

int orderQuickSort(tLista *lista) {



    lista->cabeca=orderQuickSortNo(lista->cabeca, lista->cabeca->proximo);

    return 1;

}


tNo* orderQuickSortNo(tNo *pivoNo, tNo *subListNo) {
    tNo *subListLeft=NULL;
    tNo *subListRight=NULL;
    tNo *auxNo=NULL;
    pivoNo->proximo=NULL;
    // printf("\n pivo: %d",pivoNo->conteudo);
    if(subListNo!=NULL) {
        while(subListNo!=NULL) {
            if(subListNo->conteudo<=pivoNo->conteudo) {
                //      printf("\n sublist left: %d",subListNo->conteudo);
                auxNo=subListNo;
                subListNo=subListNo->proximo;
                auxNo->proximo=subListLeft;
                subListLeft=auxNo;
            } else {
                //   printf("\n sublist rigth: %d",subListNo->conteudo);
                auxNo=subListNo;
                subListNo=subListNo->proximo;
                auxNo->proximo=subListRight;
                subListRight=auxNo;

            }
            // subListNo=subListNo->proximo;

        }

        if(subListLeft!=NULL) {

            subListLeft=orderQuickSortNo(subListLeft, subListLeft->proximo);
            tNo *auxNo2=subListLeft;
            while(auxNo2->proximo!=NULL) {

                auxNo2=auxNo2->proximo;

            }
            auxNo2->proximo=pivoNo;
            if(false) {
                printf("\n oi, aqui está a sub list left ordenado \n");

                tNo *noDoPrint=subListLeft;
                while(noDoPrint!=NULL) {
                    printf("\n%d ", noDoPrint->conteudo);
                    noDoPrint=noDoPrint->proximo;

                }

            }
        }

        if(subListRight!=NULL) {

            if(false) {
                printf("\n oi, aqui está a sub list right \n");

                tNo *noDoPrint=subListRight;
                while(noDoPrint!=NULL) {
                    printf("\n%d ", noDoPrint->conteudo);
                    noDoPrint=noDoPrint->proximo;

                }

            }

            //    printf("\n primeiro elemento da sub list rigth NAOordenada eh : %d", subListRight->conteudo);
            subListRight=orderQuickSortNo(subListRight, subListRight->proximo);
            //   printf("\n primeiro elemento da sub list rigth ordenada eh : %d", subListRight->conteudo);
            pivoNo->proximo=subListRight;
            //             printf("\n elemento apos o pivo %d eh : %d",pivoNo->conteudo, pivoNo->proximo->conteudo);
        }
        if(subListLeft!=NULL) {
            return subListLeft;
        } else {
            return pivoNo;
        }
    }

    return pivoNo;

}
/// CAMINHO ERRADO. Desisti quando vi o trabalho q ia dar pra ordenar.
/**
int insereHeap(tHeap *heap, int vetor[], int n) {
    heap->pai=new tHeapNo;
    heap->pai->noLeft=NULL;
    heap->pai->noRight=NULL;
    tHeapNo *topo=heap->pai;
    heap->pai->conteudo=vetor[0];
    printf("\n tamanho do vetor: %d\n", n);
    montaHeap(heap->pai, vetor, n, 1);

    printf("\n%d \n", heap->pai->conteudo);
    printHeap(heap->pai);

}

int printHeap(tHeapNo *no) {

    tHeapNo *left;
    tHeapNo *right;
    if(no->noLeft!=NULL && no->noRight!=NULL) {
        left=no->noLeft;
        right=no->noRight;
        printf("%d , %d\n", left->conteudo, right->conteudo);
        printf("esquerda\n");
        printHeap(left);
        printf("direita\n");
        printHeap(right);


    }

}

int montaHeap(tHeapNo *heaPai, int vetor[], int n, int i) {
    printf("2i-1 %d e 2i %d\n", (2*i), (2*i+1));
//heaPai->conteudo=vetor[i];
    if((2*i-1)<n) {

        heaPai->noLeft=new tHeapNo;
        heaPai->noLeft->noLeft=NULL;
        heaPai->noLeft->noRight=NULL;
        heaPai->noLeft->conteudo=vetor[2*i-1];
        printf("%d e valor %d\n", (2*i), vetor[2*i-1]);
        montaHeap(heaPai->noLeft, vetor, n, 2*i);
    }
    if((2*i)<n) {

        heaPai->noRight=new tHeapNo;
        heaPai->noRight->noLeft=NULL;
        heaPai->noRight->noRight=NULL;
        heaPai->noRight->conteudo=vetor[2*i];
        printf("%d e valor %d\n", (2*i+1), vetor[2*i]);
        montaHeap(heaPai->noRight, vetor, n, 2*i+1);
    }

}

**/
void max_heapify(int vet[], int n, int i) {
    int left = (2*i)+1;
    int right = (2*i)+2;
    int largest = i;
    int tmp;

    if( (left<n) && (vet[left] > vet[largest]) )
        largest = left;

    if((right<n) && (vet[right] > vet[largest]) )
        largest = right;

    if(largest != i) {
        tmp = vet[i];
        vet[i] = vet[largest];
        vet[largest] = tmp;
        max_heapify(vet, n, largest);
    }

}

void build_max_heap(int vet[], int n) {
    for (int i = n/2; i > -1; i--) {
        max_heapify(vet, n, i);
    }
}

void heap_sort(int vet[], int n) {
    build_max_heap(vet, n);
    int tam=n;
    for (int i = n-1; i >= 0; i--) {
        std::swap(vet[0], vet[i]);
        n--;
        max_heapify(vet, n, 0);
    }
    if(false) {
        for(int i=0; i<tam; i++) {
            printf("ordenado? %d \n", vet[i]);
        }
    }
}



int main (int argc, char *argv[]) {
    std::chrono::high_resolution_clock::time_point initTime;
    std::chrono::high_resolution_clock::time_point finalTime;
    //   std::cout << "fuuuuuunciona "<< std::endl;
// for (int i = 0; i < argc; ++i) {
    //    std::cout << argv[i] << std::endl;
    //  }

    if (argc != 3) {
        printf("Uso: %s <número do algoritmo> <nome do arquivo>\n", argv[0]);
        return -2;
    }

    int eita=atoi(argv[1]);
//printf("%d\n\n\n", eita);
    if(eita<1||eita>5) {
        printf("esolhar um algoritmo informando um número entre zero e cinco.");
        return -1;
    }


    string line;
    ifstream myfile (argv[2]);
    int entrada;

    getline (myfile, line);
    stringstream ss3(line);
    ss3>>entrada;

    tLista minha_lista;

    // printf("lista a ser criada: %d", entrada);
    if(eita==1||eita==2||eita==3||eita==4) {
        if(!cria(&minha_lista, entrada)) {
            return -2;
        }
    }
    //  printf("Vazia? : %d\n", vazia(minha_lista));
    //  printf("Tamanho da lista: %d \n", minha_lista.tamanho_alocado);
    if(false) {
        // int i=1;
        tNo *no=minha_lista.cabeca;
        while(no!=NULL) {
            printf("%d \n", no->conteudo);
            no=no->proximo;

        }
        printf("criada, ta ai a prova. agora vamos começar. \n");
    }



    if (myfile.is_open()) {


        if(eita==5) {
            int n=entrada;
            int vect[n];
            int i=0;
            while ( getline (myfile, line) ) {

                stringstream ss3(line);
                ss3>>entrada;
                vect[i]=entrada;
                // printf("%d\n",entrada);
                i++;
            }
            myfile.close();

            initTime = std::chrono::high_resolution_clock::now();

            heap_sort(vect, n);

            finalTime = std::chrono::high_resolution_clock::now();

            std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(finalTime - initTime).count() << std::endl;



            return -8;

        }

        if(eita==2) {

            initTime = std::chrono::high_resolution_clock::now();
        }

        while ( getline (myfile, line) ) {

            stringstream ss3(line);
            ss3>>entrada;


            if(eita==2) {

                insertionSort(&minha_lista, entrada);

            }


            if(eita==1 || eita==4||eita==3) {
                insere(&minha_lista, minha_lista.tamanho+1, entrada);
            }

        }
        myfile.close();
        if(eita==2) {
            finalTime = std::chrono::high_resolution_clock::now();

            std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(finalTime - initTime).count() << std::endl;


        }

//std::cout << " teste" << std::endl;
        if(eita==4) {


            initTime = std::chrono::high_resolution_clock::now();
            orderQuickSort(&minha_lista);
            finalTime = std::chrono::high_resolution_clock::now();

            std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(finalTime - initTime).count() << std::endl;
        }

        if(eita==3) {

            initTime = std::chrono::high_resolution_clock::now();
            orderMergeSort(&minha_lista);
            finalTime = std::chrono::high_resolution_clock::now();

            std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(finalTime - initTime).count() << std::endl;
        }
        if(eita==1) {

            initTime = std::chrono::high_resolution_clock::now();
            orderSelectionSort(&minha_lista);
            finalTime = std::chrono::high_resolution_clock::now();

            std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(finalTime - initTime).count() << std::endl;
        }
        // printf("\n");
        if(false) {
            // int i=1;
            tNo *no=minha_lista.cabeca;
            while(no!=NULL) {
                printf("%d \n", no->conteudo);
                no=no->proximo;

            }

        }
        //  delete &minha_lista;

        return 0;
    }
}
